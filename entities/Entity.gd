extends Node2D
class_name Entity


var sight:EntitySight = null;
var movement:EntityMovement = null;
var behaviour:EntityBehaviour = null;
var appearance:EntityAppearance = null;
var _physics_body:PhysicsBody2D = null;

var _entity_data:Dictionary;

func setup(entity_data:Dictionary, sight_class:GDScript, movement_class:GDScript, behaviour_class:GDScript):
	
	_entity_data = entity_data;
	set_name(_entity_data['name']);
	
	if(_entity_data['appearance_class'] != null && _entity_data['appearance_class'] != ""):
		var appearance_class = load("res://"+_entity_data['appearance_class']+".gd");
		var appearance_set_path = "res://entities/appearances/"+_entity_data['appearance_id']+".tscn";
		var appearance_set = load(appearance_set_path).instance();
		appearance_set.set_name("Appearance");
		appearance_set.script = appearance_class;
		appearance = appearance_set;
		add_child(appearance_set);
		appearance.setup(self);
		appearance.load_data(_entity_data);
	
	if(sight_class):
		sight = $Sight;
		sight.script = sight_class;
		sight.setup(self);
		sight.name = 'EntitySight';
		add_child(sight);
	
	if(movement_class):
		movement = Node.new();
		movement.script = movement_class;
		movement.name = 'EntityMovement';
		movement.setup(self);
		add_child(movement);
	
	if(behaviour_class):
		behaviour = Node.new();
		behaviour.script = behaviour_class;
		behaviour.setup(self);
		add_child(movement);
	
	_post_setup();
	
func _post_setup():
	#TODO - EntityUnit class for specifics
#	if(appearance != null):
#		_physics_body = $Appearance/Skeleton;
#		_physics_body.set_script(load("res://entities/appearances/Skeleton.gd"));
#		_physics_body.entity = self;
	pass;



func update_entity(delta:float)->void:
	if(movement):
		movement.update(delta);
	if(sight):
		sight.update(delta);
	if(behaviour):
		behaviour.update(delta);

func set_facing_direction(dir:int)->void:
		scale.x = dir * -1;

	