extends EntityAppearance

var sprite:Sprite;
var text:RichTextLabel;
var area:Area2D;

func _post_setup()->void:
	sprite = get_node("Sprite");
	text = $Control/RichTextLabel;
	area = $Area2D;
	$VisibilityNotifier2D.connect("screen_entered", self, "on_screen_entered");
	area.connect("mouse_entered", self, "on_mouse_entered");

func load_data(data:Dictionary)->void:
	sprite.set_texture(load("res://sprites/sheet_01_godot.sprites/sheet_01/decor/"+data['sprite_id_head']+".tres"));

func on_screen_entered()->void:
	text.set_text("Who the hell goes there?");

func on_mouse_entered()->void:
	text.set_text("Get your paws off me you mangy fuck");
