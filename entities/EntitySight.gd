extends Node
class_name EntitySight

var entity = null;
onready var _area_2d:Area2D;

func setup(entity):
	self.entity = entity;
	_area_2d = get_node("./LineOfSight");
	_area_2d.connect("body_entered", self, "on_body_entered");
	_area_2d.connect("body_exited", self, "on_body_exited");

func update(delta:float):
	pass;
	
func is_in_sight(id:String):
	return false;
	
func on_body_entered(body:PhysicsBody2D)->void:
	return;
	
func on_body_exited(body:PhysicsBody2D)->void:
	return;
	