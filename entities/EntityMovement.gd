extends Node
class_name EntityMovement

export(float) var arrive_radius = 10;
export(Vector2) var velocity = Vector2.ZERO;
export(float) var max_velocity = 3;

export(Vector2) var target_position = null;
export(float) var random_offset_max = 5;

var entity = null;
var _path:Array = [];
var path_id:int = -1;
var path_node_index:int = -1;
var random_offset:Vector2 = Vector2.ZERO;

func setup(entity):
	self.entity = entity;
	
	random_offset = Vector2(randi() % int(random_offset_max), randi() % int(random_offset_max));
	if(randi() % 2 == 1):
		random_offset.x = -random_offset.x;
	if(randi() % 2 == 1):
		random_offset.y = -random_offset.y;


func update(delta:float):
	update_steering();
	move(velocity);
	update_facing_direction();
	
func update_facing_direction():
	if(velocity.x > 0):
		entity.set_facing_direction(1);
	else:
		entity.set_facing_direction(-1);		
	
func follow_path():
	if(_path.size() > 0):
		if(target_position != null && is_at(target_position)):
			path_node_index += 1;
		if(path_node_index < _path.size()):
			var new_pos = _path[path_node_index];
			target_position = new_pos + random_offset;
	
func is_at(pos)->bool:
	var desired_velocity = pos - entity.position
	var distance = desired_velocity.length();
	return distance < arrive_radius;
	


func move(velocity:Vector2)->void:
	entity.position += velocity;
	
func update_steering()->void:
	if(target_position != null):
		seek();
		arrive();
	
func seek():
	if(target_position != null):
		var desired_velocity = (target_position-entity.position).normalized() * max_velocity;
		var steering = desired_velocity - velocity;
		velocity = truncate(velocity + steering, max_velocity);

func arrive()->void:
	var desired_velocity = target_position - entity.position
	var distance = desired_velocity.length();
 
	if (distance < arrive_radius):
	    desired_velocity = desired_velocity.normalized() * max_velocity * (distance / arrive_radius)
	else:
    	desired_velocity = desired_velocity.normalized() * max_velocity
 
	var steering = desired_velocity - velocity
	velocity = truncate(velocity + steering, max_velocity);


func truncate(vector:Vector2, max_amount:float)->Vector2:
	var i :float;
	if(vector.length() > 0):
		i = max_amount / vector.length();
		i = i if i < 1 else 1;
		vector *= i;
	return vector;

func set_path(path:Array, path_id:int)->void:
	_path = path;
	self.path_id = path_id;
	path_node_index = get_closest_path_node_index();

	
func get_closest_path_node_index()->int:
	if(_path != null):
		var closest_distance = 9999999999;
		var closest_index = -1;
		for index in range(_path.size()):
			var pos = _path[index];
			var distance = Vector2(pos.x,pos.y).distance_to(entity.position);
			if(distance <= closest_distance):
				closest_index = index;
				closest_distance = distance;

		return closest_index;
	return -1;