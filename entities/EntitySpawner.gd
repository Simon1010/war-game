extends Position2D
class_name EntitySpawner

export(NodePath) var entity_container_path:NodePath;

onready var entity_scene = preload('res://entities/Entity.tscn');
onready var entity_container = get_node(entity_container_path);

var unit_paths = [];
var entities = [];

func _ready():
	pass;

func spawn_entity(x:float, y:float):
	var random_entity_id = randi() % 2 + 2;
	
	var entity_data = static_data.units.values()[random_entity_id];
	var entity = entity_scene.instance();
	entity_container.add_child(entity);
	entity.set_position(Vector2(x,y));

	var movement_class = preload("res://entities/EntityMovement.gd");
	var sight_class = preload("res://entities/EntitySight.gd");
	var behaviour_class = preload("res://entities/behaviour/BehaviourWarrior.gd");
	
	entity.setup(entity_data, sight_class, movement_class, behaviour_class);

	var path_id = randi()%unit_paths.size();
	entity.movement.set_path(unit_paths[path_id], path_id);
	entities.append(entity);
	
	
func spawn_entity_custom(x:float, y:float, entity_id:int):
	var entity_data = static_data.units.values()[entity_id];
	var entity = entity_scene.instance();
	entity_container.add_child(entity);
	entity.set_position(Vector2(x,y));

	var movement_class = null;
	var sight_class = preload("res://entities/EntitySight.gd");
	var behaviour_class = null;
	
	entity.setup(entity_data, sight_class, movement_class, behaviour_class);
	entities.append(entity);
	
func update_entities(delta:float):
	for entity in entities:
		entity.update_entity(delta);

func paths_updated():
	for entity in entities:
		if(entity.movement != null):
			var path_id = entity.movement.path_id;
			var previous_path_node_index = entity.movement.path_node_index;
			entity.movement.set_path(unit_paths[path_id], path_id);
			entity.movement.path_node_index = previous_path_node_index;
		