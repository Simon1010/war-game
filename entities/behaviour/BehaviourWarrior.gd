extends EntityBehaviour
class_name BehaviourWarrior

signal began_walking();
signal began_idle();
signal finished_idle();

var entity:Entity = null;

func setup(entity:Entity):
	self.entity = entity;
	self.entity.appearance.connect("animation_finished", self, "on_animation_finished");

func update(delta:float):
	if(Input.is_action_just_released("attack_units_temp")):
		if(set_state(STATE_ATTACKING)):
			entity.appearance.play(EntityAppearance.ATTACK_01);
		
	if(is_state_blocking() == false):
		if(entity.sight.is_in_sight('ally') == false):
			entity.movement.follow_path();
			if(set_state(STATE_WALKING)):
				entity.appearance.play(EntityAppearance.WALK_01);
		else:
			if(set_state(STATE_IDLE)):
				entity.appearance.play(EntityAppearance.IDLE_01);
			
func on_animation_finished(appearance_name:String):
	if(get_state() == STATE_ATTACKING):
		set_state(STATE_NONE);