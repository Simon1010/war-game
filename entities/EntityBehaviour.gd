extends Node
class_name EntityBehaviour

enum {
	STATE_NONE,
	STATE_IDLE, 
	STATE_WALKING, 
	STATE_ATTACKING,
	};
	
var _current_state = -1;

var _blocking_states = [STATE_ATTACKING];

func _ready():
	pass
	
func set_state(state:int)->bool:
	if(_current_state != state):
		var previous_state = _current_state;
		_current_state = state;
		_on_state_changed(previous_state);	
		return true;
	return false;

func get_state()->int:
	return _current_state;
	
func _on_state_changed(previous_state:int)->void:
	pass;
	
func is_state_blocking()->bool:
	return _blocking_states.has(_current_state);