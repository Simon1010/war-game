extends Node2D
class_name EntityAppearance

signal animation_finished(anim_name)

const IDLE_01 = "idle_01";
const WALK_01 = "walk_01";
const ATTACK_01 = "attack_01";
var animation_player:AnimationPlayer;
var _skeleton:Node2D;

func setup(entity):
	animation_player = $AnimationPlayer;
	if(animation_player != null):
		animation_player.connect("animation_finished", self, "_on_animation_finished");
	_skeleton = $Skeleton;
	_post_setup();

func _post_setup():
	pass;

func play(animation_name:String):
	animation_player.play(animation_name);

func _on_animation_finished(animation_name:String):
	emit_signal("animation_finished", animation_name);
	