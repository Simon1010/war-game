extends Node2D
class_name GridNode

signal mouse_entered_node(nodeSelf);
signal mouse_exited_node(nodeSelf);
signal node_changed(nodeSelf);

var weight:int = 0;


func set_move_cost(new_value):
	emit_signal("node_changed", self);
	

func _on_Area2D_mouse_entered():
	emit_signal("mouse_entered_node", self);


func _on_Area2D_mouse_exited():
	emit_signal("mouse_exited_node", self);
