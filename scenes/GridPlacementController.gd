extends Node2D
class_name GridPlacementController

signal placement_item_created
signal placement_item_destroyed
signal placement_item_picked_up
signal placement_item_released
signal placement_item_moved

export(NodePath) var camera_path:NodePath;
export(NodePath) var grid_path:NodePath;
export(NodePath) var battle_map_path:NodePath;

var current_item:PlaceableItem = null;
var current_node:GridNode = null;
var previous_node:GridNode = null;


var _occupied_nodes:Array = [];

var grid:Grid = null;
var camera:Camera2D = null;
var battle_map = null;

func setup():
	camera = get_node(camera_path);
	grid = get_node(grid_path);
	battle_map = get_node(battle_map_path);
	_load_occupied_nodes();

func is_placing_item():
	return current_item != null;

func _process(delta):
	update_pan_screen();
	
	if(current_item != null and current_item.picked_up):
		if(current_node != null):
			set_item_pos_to_node();
		else:
			set_item_pos(get_global_mouse_position());

#TODO - Change Sprite to ItemData? 
func set_placement_item(scene_path:String)->void:
	if(current_item == null):
		current_item = preload("res://scenes/PlaceableItem.tscn").instance();
		
		var random_sprites = ["tower_barricade_001","tower_hole_001","tower_ranged_001"];
		randomize();
		var i = randi() % random_sprites.size();
		var random_sprite = random_sprites[i];
		if(random_sprite == "tower_ranged_001"):
			current_item.grid_offset = Vector2(0, -5);
		if(random_sprite == "tower_hole_001"):
			current_item.grid_offset = Vector2(0, 0);
		if(random_sprite == "tower_barricade_001"):
			current_item.grid_offset = Vector2(0, 0);
			
		current_item.get_node("Item/Sprite").set_texture(load('res://sprites/sheet_01_godot.sprites/sheet_01/defenses/'+random_sprite+'.tres'));
		add_child(current_item);
		current_item.set_name("PlaceableItem");
		current_item.connect('picked_up_item', self, '_on_PlaceableItem_picked_up_item');
		current_item.connect('dropped_item', self, '_on_PlaceableItem_dropped_item');
		emit_signal("placement_item_created");

		var grid_pos = grid.to_grid_units(Vector2(randi()%2500,400));
		current_node = grid.get_grid_nodes()[grid_pos.x][grid_pos.y];
		set_item_pos_to_node();
		_load_occupied_nodes();
		

func remove_placement_item()->void:
	if(current_item):
		current_item.queue_free();
		current_item = null;
		emit_signal("placement_item_destroyed");
		
	
func update_pan_screen():
	if(current_item != null):
		var screen_position = current_item.get_global_transform_with_canvas().origin;
		
		var edge_offset = 100;
		var left_edge = edge_offset;
		var right_edge = get_viewport().get_size().x - edge_offset;
		var distance_to_edge_perc = 0;
		var speed_multiplier = 2;
		var speed = 5;
		
		if(screen_position.x < left_edge):
			distance_to_edge_perc = 1 - (screen_position.x / left_edge);
			speed *= distance_to_edge_perc * speed_multiplier;
			camera.translate(Vector2(-speed, 0));
		if(screen_position.x > right_edge):
			var distance_from_edge = abs(get_viewport().get_size().x - right_edge);
			distance_to_edge_perc = abs(screen_position.x - right_edge) / distance_from_edge;
			speed *= distance_to_edge_perc * speed_multiplier;
			camera.translate(Vector2(speed, 0));


func reparent_item(new_parent:Object):
	var old_pos = current_item.global_position;
	current_item.get_parent().remove_child(current_item);
	new_parent.add_child(current_item);
	current_item.set_global_position(old_pos);

func _on_PlaceableItem_picked_up_item(item:PlaceableItem):
	current_item = item;
	reparent_item(camera);
	emit_signal("placement_item_picked_up");

func _on_PlaceableItem_dropped_item(item:PlaceableItem):
	reparent_item(self);
	emit_signal("placement_item_released");

func _on_Grid_node_mouse_entered(node):
	current_node = node;
	
func _on_Grid_node_mouse_exited(node):
	if current_node == node:
		current_node = null;

func set_item_pos(pos):
	if(current_item.get_global_position() != pos):
		current_item.set_global_position(pos);
		emit_signal("placement_item_moved");

func set_item_pos_to_node():
	var node_pos = current_node.global_position;
	var item_node_offset = current_item.grid_offset;
	node_pos += item_node_offset * grid.get_size();
	node_pos.x -= grid.get_size()/2;
	node_pos.y -= grid.get_size()/2;
	set_item_pos(node_pos);

func set_node(new_node):
	previous_node = current_node;
	current_node = new_node;
	
	
func get_synced_occupied_nodes()->Array:
	return _load_occupied_nodes();
	
func _load_occupied_nodes()->Array:
	_occupied_nodes = grid.generate_2d_array_with_grid_size();
	for x in _occupied_nodes.size():
		for y in _occupied_nodes[x].size():
			_occupied_nodes[x][y] = 0;

	var items = battle_map.placed_items;
	for item in items:
		var item_grid_units = grid.get_size_in_unit_map(item.get_size());
		var item_grid_position = grid.to_grid_units(item.get_global_position());

		var positions = grid.get_grid_position_from_top_left(item_grid_position, item_grid_units);
		for position in positions:
			if grid.is_within_bounds(position):
				_occupied_nodes[position.x][position.y] = 1;

	
	return _occupied_nodes;

func get_occupied_nodes()->Array:
	return _occupied_nodes;
	
func get_overlapping_nodes()->Array:
	var overlapping_nodes = [];
	var item_nodes = grid.get_size_in_unit_map(current_item.get_size());	
	var item_grid_pos = grid.to_grid_units(current_item.get_global_position());

	var positions = grid.get_grid_position_from_top_left(item_grid_pos, item_nodes);
	for position in positions:
		if(_occupied_nodes[position.x][position.y] == 1):
			overlapping_nodes.append(position);
	return overlapping_nodes

func can_fit_item()->bool:
	return get_overlapping_nodes().size() == 0;
	

