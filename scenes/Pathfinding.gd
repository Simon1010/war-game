extends Node2D
class_name Pathfinding

export (NodePath) var grid_path:NodePath;

onready var grid:Grid = get_node(grid_path);

var _setup:bool = false;

var astar = null;

func setup():
	_setup = true;
	astar = AStar.new();	


func find_path(start:Vector2, end:Vector2, toWorld:bool = true)->Array:
	grid.sync_nodes_with_placements();
	if(_setup == false):
		printerr('Pathfinding not setup! Please call setup()');
	var _nodes = grid.get_grid_nodes();
	for x in _nodes.size():
		for y in _nodes[x].size():
			astar.add_point(get_node_id(Vector2(x,y)), Vector3(x,y, 0), _nodes[x][y].weight);
	_connect_traversable_tiles();
	
	var path =  astar.get_point_path(get_node_id(start), get_node_id(end));
	if(toWorld == false):
		return path;
		
	var world_path = [];
	for index in range(path.size()):
		var pos = path[index];
		pos = Vector2(pos.x,pos.y);
		world_path.append(grid.to_world_units(pos));
	
	return world_path;
	
func _connect_traversable_tiles():

	var nodes = grid.get_grid_nodes();
	# Loop over all tiles
	for x in nodes.size():
		for y in nodes[x].size():

			# Determine the ID of the tile
			var id = get_node_id_with_int(x,y);
	
			# Loops used to search around player (range(3) returns 0, 1, and 2)
			for offset_x in range(3):
				for offset_y in range(3):
	
					# Determines target, converting range variable to -1, 0, and 1
					var target = Vector2(x,y) + Vector2(offset_x - 1, offset_y - 1)
					
					if(target.x < 0 or target.x >= grid.get_columns() or target.y < 0 or target.y >= grid.get_rows()):
						continue;
	
					# Determines target ID
					var target_id = get_node_id(target)
	
					# Do not connect if point is same or point does not exist on astar
					if id == target_id or not astar.has_point(target_id):
						continue


					# Connect points
					astar.connect_points(id, target_id)

func get_node_id_with_int(a:int, b:int)->int:
	return get_node_id(Vector2(a,b));
	
func get_node_id(grid_pos:Vector2)->int:
	var column = grid_pos.x;
	var row = grid_pos.y * grid.get_columns();
	var new_id = column + row;
	return new_id;
