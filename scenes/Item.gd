extends Node2D
class_name Item

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_item_sprite(sprite:Texture):
	$Sprite.set_texture(sprite);
	
func get_size():
	var size = $Sprite.get_texture().get_size();
	return size;
