extends Node2D
class_name GridNodeViewer

const DEFAULT_COLOR = Color(1,1,1, 0.3);
const SELECTED_COLOR = Color(1,1,1, 0.7);
const HOVER_COLOR = Color(1,1,1, 0.8);
const OVERLAPPING_COLOR = Color(1,0,0, 0.7);


const DEFAULT_ALPHA = 0.3;
const HOVER_ALPHA = 0.5;
const SELECTED_ALPHA = 0.7;

var grid:Grid = null;
var drawn_nodes:Array = [];
var changed_nodes:Array = [];

func _ready() -> void:
	grid = get_parent();
	drawn_nodes = grid.generate_2d_array_with_grid_size();
	changed_nodes = grid.generate_2d_array_with_grid_size();

		
func delete_nodes() -> void:
	if(drawn_nodes != null):
		for x in range(drawn_nodes.size()):
			for y in range(drawn_nodes[x].size()):
				destroy_node(x,y);
	
	drawn_nodes = grid.generate_2d_array_with_grid_size();
	changed_nodes = grid.generate_2d_array_with_grid_size();


func create_nodes() -> void:
	#Creating the nodes
	for x in drawn_nodes.size():
		for y in drawn_nodes[x].size():
			create_node(x,y);
			changed_nodes[x][y] = 0;

func destroy_node(x:int, y:int)->void:
	if(drawn_nodes[x][y] != null):
		drawn_nodes[x][y].queue_free();	
	

func create_node(x:int, y:int)->void:
	var node = load('res://scenes/GridNodeDrawn.tscn').instance();
	node.set_name('Node'+str(x)+'x'+str(y));
	add_child(node);
	node.position = Vector2(x * grid.grid_data['size'], y * grid.grid_data['size']);
	drawn_nodes[x][y] = node;
	set_node_color(x,y, DEFAULT_COLOR);
				
	
func reset_color()->void:
	for x in changed_nodes.size():
		for y in changed_nodes[x].size():
			if(changed_nodes[x][y] == 1):
				set_node_color(x,y, DEFAULT_COLOR);
				changed_nodes[x][y] = 0;
				

func set_grid_color(start_node:GridNode, offsets_to_draw:Array, color:Color) -> void:
	var grid_pos = grid.to_grid_units(start_node.global_position);
	
	var positions = grid.get_grid_position_from_top_left(grid_pos, offsets_to_draw);
	for position in positions:
		if grid.is_within_bounds(position):
			set_node_color(position.x,position.y, color);

func set_node_color(x:int, y:int, color:Color) -> void:
	var drawn_node = drawn_nodes[x][y];
	var drawn_node_sprite = drawn_node.get_node('Sprite');
	drawn_node_sprite.modulate = color;
	changed_nodes[x][y] = 1;
