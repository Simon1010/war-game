extends Camera2D

const SPEED = 50;
const ZOOM_SPEED = 0.05;
const PAN_SPEED = 1.5;

var velocity = Vector2();
var direction = Vector2();
var dragging = false;
var is_user_interaction_disabled:bool = false;

func get_camera_width():
	return get_viewport().size.x * zoom.x;

func get_camera_height():
	return get_viewport().size.y * zoom.y;
	
func _process(delta):
	if(is_user_interaction_disabled == false):
		direction = Vector2.ZERO;
		if(Input.is_action_pressed("ui_left")):
			direction.x = -1;
		if(Input.is_action_pressed("ui_right")):
			direction.x = 1;
		if(Input.is_action_pressed("ui_up")):
			direction.y = -1;
		if(Input.is_action_pressed("ui_down")):
			direction.y = 1;
		
		velocity = SPEED * direction.normalized();
		translate(velocity);
	
	manageBounds();
	
	
	
func _input(event):
	if(is_user_interaction_disabled == false):
		var camera_zoom  = get_zoom();
		if event is InputEventMouseButton:
			if event.is_pressed():
				dragging = true;
				if event.button_index == BUTTON_WHEEL_UP:
					camera_zoom += (ZOOM_SPEED * -Vector2.ONE);
				if event.button_index == BUTTON_WHEEL_DOWN:
					camera_zoom += (ZOOM_SPEED * Vector2.ONE);
			else:
				dragging = false;
		elif event is InputEventMouseMotion and dragging:
			translate(-event.relative * PAN_SPEED);
		setZoom(camera_zoom);
	else:
		dragging = false;
		
	
func setZoom(zoom):
	if(zoom.x < 0.5):
		zoom = (Vector2(0.5,0.5))
	if(zoom.x > 1):
		zoom = (Vector2(1, 1))
	set_zoom(zoom);

func manageBounds():
	var cameraWidth = get_camera_width();
	var cameraHeight = get_camera_height();
	
	var cameraMaxLeft = (limit_left+cameraWidth/2);
	var cameraMaxRight = (limit_right-cameraWidth/2);
	var cameraMaxTop = (limit_top+cameraHeight/2);
	var cameraMaxBottom = (limit_bottom-cameraHeight/2);
	
	if(position.x < cameraMaxLeft):
		position.x = cameraMaxLeft;
		
	if(position.x > cameraMaxRight):
		position.x = cameraMaxRight;
		
	if(position.y < cameraMaxTop):
		position.y = cameraMaxTop;
		
	if(position.y > cameraMaxBottom):
		position.y = cameraMaxBottom;

func toggle_smooth(smooth:bool):
	limit_smoothed = smooth;
	smoothing_enabled = smooth;


func _on_GridPlacementController_placement_item_picked_up():
	is_user_interaction_disabled = true;
	toggle_smooth(false);


func _on_GridPlacementController_placement_item_released():
	is_user_interaction_disabled = false;
	toggle_smooth(true);
