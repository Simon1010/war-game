extends GridNodeViewer

export(NodePath) var pathfinding_path:NodePath;

var pathfinding:Pathfinding = null;

# Called when the node enters the scene tree for the first time.
func _ready():
	pathfinding = get_node(pathfinding_path);
	pass # Replace with function body.
	

func redraw():
	
	delete_nodes();
	
	var grid_nodes = grid.get_grid_nodes();
	var start = Vector2(0,5);
	var end = Vector2(50,7);
	var path = pathfinding.find_path(start,end,false);
			
	for position in path:
		var node = grid_nodes[position.x][position.y];
		create_node(position.x, position.y);
		set_node_color(position.x,position.y, Color.blue);
		
func _debug_node(node:GridNode)->void:
	var x = node.position.x;
	var y = node.position.y;
	var drawn_node = get_node(node.name);
	drawn_node.get_node('Pos_Text').set_text(str(x) + 'x' + str(y));
	drawn_node.get_node('Pos_Text').set_text('');
	drawn_node.get_node('Id_Text').set_text(str(pathfinding.astar.get_point_weight_scale(pathfinding.get_node_id_with_int(x,y))));
	var connection_string = "";
	for point in pathfinding.astar.get_point_connections(pathfinding.get_node_id_with_int(x,y)):
		connection_string += str(point)+",";	
	drawn_node.get_node('Connections_Text').set_text(connection_string);
							
