extends GridNodeViewer

export(NodePath) var grid_placement_controller_path:NodePath;

var grid_placement_controller:GridPlacementController = null

func get_current_item()->PlaceableItem:
	return grid_placement_controller.current_item;
	
func get_current_node()->GridNode:
	return grid_placement_controller.current_node;

func _ready():
	grid_placement_controller = get_node(grid_placement_controller_path);
	
func redraw():
	reset_color();
	
	var grid_nodes = grid.get_grid_nodes();
	var occupied_nodes = grid_placement_controller.get_occupied_nodes();
	for x in occupied_nodes.size():
		for y in occupied_nodes[x].size():
			if(occupied_nodes[x][y] == 1):
				set_node_color(x, y, Color(1,0,0,0.4));
				
	
	if(get_current_item() and get_current_node()):
		var offsets = grid.get_size_in_unit_map(get_current_item().get_size());
		var item_grid_pos = grid.to_grid_units(get_current_item().get_global_position());
		var grid_positions = grid.get_grid_position_from_top_left(item_grid_pos, offsets);
		var color = HOVER_COLOR;
		for position in grid_positions:
			var is_node_occupied = occupied_nodes[position.x][position.y] == 1;
			set_node_color(position.x,position.y, HOVER_COLOR if is_node_occupied == false else OVERLAPPING_COLOR);


func _on_GridPlacementController_placement_item_created():
	create_nodes();
	redraw();


func _on_GridPlacementController_placement_item_picked_up():
	redraw();


func _on_GridPlacementController_placement_item_moved():
	redraw();


func _on_GridPlacementController_placement_item_released():
	redraw();


func _on_GridPlacementController_placement_item_destroyed():
	delete_nodes();