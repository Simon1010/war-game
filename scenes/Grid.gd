extends Node2D
class_name Grid

signal node_mouse_entered(node)
signal node_mouse_exited(node)
signal node_mouse_clicked(node)
signal grid_changed()

var _nodes = [];

export(Dictionary) var grid_data:Dictionary;
export(NodePath) var placement_controller_path:NodePath;

onready var placement_controller = get_node(placement_controller_path);

func setup():
	create_nodes();
	print('setup grid');

func get_columns()->int:
	return grid_data['columns'];

func get_rows()->int:
	return grid_data['rows'];

func get_size()->int:
	return grid_data['size'];
	
func generate_2d_array_with_grid_size()->Array:
	var new_array = [];
	for x in get_columns():
		new_array.append([]);
		for y in get_rows():
			new_array[x].append([]);
			new_array[x][y] = null;
	
	return new_array;

func create_nodes():
	_nodes = [];
	for x in grid_data['columns']:
		_nodes.append([]);
		for y in grid_data['rows']:
			_nodes[x].append([]);
			
			#Creating the nodes
			var node = load('res://scenes/GridNode.tscn').instance() as GridNode;
			node.set_name('Node'+str(x)+'x'+str(y));
			$Nodes.add_child(node);
			_nodes[x][y] = node;
			node.connect('node_changed', self, 'on_GridNode_node_changed');
			node.connect('mouse_entered_node', self, 'on_GridNode_mouse_entered_node');
			node.connect('mouse_exited_node', self, 'on_GridNode_mouse_exited_node');
			node.get_node("Area2D/CollisionShape2D").get_shape().extents = Vector2(grid_data['size']/2,grid_data['size']/2);
			node.position = Vector2(x * grid_data['size'], y * grid_data['size']);
			
func to_grid_units(world_units:Vector2) -> Vector2:
	world_units.x -= position.x;
	world_units.y -= position.y;
	
	var grid_x = int(ceil(world_units.x / grid_data['size']));
	var grid_y = int(ceil(world_units.y / grid_data['size']));
	
	return Vector2(grid_x, grid_y);
	
func to_world_units(grid_units:Vector2) -> Vector2:
	var world_x = grid_units.x * grid_data['size'];
	var world_y = grid_units.y * grid_data['size'];
	world_x += position.x;
	world_y += position.y;
	return Vector2(world_x, world_y);

func get_grid_position_from_top_left(start:Vector2, offsets:Array)->Array:
	var array = [];
	for x in offsets.size():
		for y in offsets[x].size():
			var offset_x = int(round(offsets.size()/2));
			var offset_y = int(round(offsets[x].size()/2));
			offset_x = 0;
			offset_y = 0;
			var x_index = start.x - offset_x + x;
			var y_index = start.y - offset_y + y;
			var grid_pos = Vector2(x_index, y_index);
			if is_within_bounds(grid_pos):
				array.append(grid_pos);
	return array;
	
func snap_world_units(world_units:Vector2)->Vector2:
	return to_world_units(to_grid_units(world_units));

func get_size_in_unit_map(world_units:Vector2) -> Array:
	
	var size_in_grid_units = to_grid_units(world_units);
	var size_map = [];
	for x in size_in_grid_units.x:
		size_map.append([]);
		for y in size_in_grid_units.y:
			size_map[x].append([]);
	
	return size_map;
	
func is_within_bounds(grid_position: Vector2)->bool:
	var is_x_valid =  (grid_position.x >= 0 && grid_position.x < grid_data['columns']);
	
	var is_y_valid =  (grid_position.y >= 0 && grid_position.y < grid_data['rows']);
	return is_x_valid && is_y_valid;

#Signals
func on_GridNode_mouse_entered_node(node: GridNode):
	emit_signal("node_mouse_entered", node);
		

func on_GridNode_mouse_exited_node(node: GridNode):
	emit_signal("node_mouse_exited", node);
	
func get_grid_nodes()->Array:
	return _nodes;

func get_grid_node(column:int, row:int)->GridNode:
	return _nodes[column][row];	

func grid_changed():
	emit_signal('grid_changed');
	
func sync_nodes_with_placements():
	var occupied_nodes = placement_controller.get_synced_occupied_nodes();
	
	var grid_nodes = _nodes;
	for x in grid_nodes.size():
		for y in grid_nodes[x].size():
			grid_nodes[x][y].weight = 1;
			if occupied_nodes[x][y] == 1:
				grid_nodes[x][y].weight = 999;