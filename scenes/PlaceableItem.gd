extends Node2D
class_name PlaceableItem

signal picked_up_item(item)
signal dropped_item(item)
signal mouse_entered_item(item)
signal mouse_exited_item(item)

var picked_up:bool = false;
var current_node:GridNode = null;
var item:Item = null;
var grid_offset:Vector2 = Vector2(0,0);

# Called when the node enters the scene tree for the first time.
func _ready():
	item = $Item;
	$Area2D/CollisionShape2D.get_shape().set_extents(item.get_size()/2);
	$Area2D.set_position(item.get_size()/2);
	pass # Replace with function body.


func _on_Area2D_mouse_entered():
	emit_signal('mouse_entered_item', self);


func _on_Area2D_mouse_exited():
	emit_signal('mouse_exited_item', self);


func _on_Area2D_input_event(viewport, event, shape_idx):
	if(event is InputEventMouseButton):
		if(event.button_index == BUTTON_LEFT):
			if(event.pressed):
				pick_up_item();
			else:
				if(picked_up):
					drop_item();
					
func pick_up_item():
	emit_signal('picked_up_item', self);	
	picked_up = true;
	pass;
	
func drop_item():
	emit_signal('dropped_item', self);	
	picked_up = false;
	pass;

func _input(event):
	if(event is InputEventMouseButton):
		if(event.button_index == BUTTON_LEFT):
			if(event.pressed == false):
				if(picked_up):
					drop_item();

func get_size():
	return item.get_size();
