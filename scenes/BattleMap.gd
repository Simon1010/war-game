extends Node2D
class_name BattleMap

export(NodePath) var camera_path:NodePath;
export(NodePath) var pathfinding_path:NodePath;
export(NodePath) var grid_path:NodePath;
export(NodePath) var grid_placement_path:NodePath;
export(NodePath) var entity_container_path:NodePath;
export(NodePath) var entity_spawner_path:NodePath;

onready var grid:Grid = get_node(grid_path);
onready var pathfinding:Pathfinding = get_node(pathfinding_path);
onready var camera:Camera2D = get_node(camera_path);
onready var grid_placement:GridPlacementController = get_node(grid_placement_path);
onready var entity_spawner:EntitySpawner = get_node(entity_spawner_path);
onready var entity_container:Node = get_node(entity_container_path);

var placed_items:Array = [];

func _ready():
	grid.setup();
	pathfinding.setup();
	grid_placement.setup();
	update_unit_paths();
	_spawn_units();
	
	entity_spawner.spawn_entity_custom(1600,500,4);
	
func update_unit_paths():
	entity_spawner.unit_paths.clear();
	entity_spawner.unit_paths.append(pathfinding.find_path(Vector2(0, 4), Vector2(75, 5)));
	entity_spawner.unit_paths.append(pathfinding.find_path(Vector2(0, 6), Vector2(75, 6)));
	entity_spawner.unit_paths.append(pathfinding.find_path(Vector2(0, 12), Vector2(75, 6)));
	entity_spawner.unit_paths.append(pathfinding.find_path(Vector2(0, 10), Vector2(75, 7)));
	entity_spawner.paths_updated();


func _spawn_units():
	for i in range(2):
		entity_spawner.spawn_entity((randi()%1500 + 100), randi()%800 + 100);

func _process(delta:float)->void:
	if(grid_placement.is_placing_item() == false):
		entity_spawner.update_entities(delta);
		
	if(Input.is_action_just_released("spawn_units_temp")):
		_spawn_units();

func _on_BuyButton_button_up():
	var current_item = $Elements/Grid/GridPlacementController.current_item;
	if(current_item):
		if($Elements/Grid/GridPlacementController.can_fit_item()):
			$Elements/Grid/GridPlacementController.remove_placement_item();
			var item = preload("res://scenes/Item.tscn").instance();
			placed_items.append(item);
			item.set_name("PlacedItem");
			entity_container.add_child(item);
			item.set_global_position(current_item.get_global_position());
			item.set_item_sprite(current_item.item.get_node("Sprite").texture);
			grid.grid_changed();
	
func _on_MoreButton_button_up():	
	if $Elements/Grid/GridPlacementController.current_item == null:
		$Elements/Grid/GridPlacementController.set_placement_item("");
#

func _on_Grid_grid_changed():
	update_unit_paths();
