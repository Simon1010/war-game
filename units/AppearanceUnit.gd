extends EntityAppearance
class_name AppearanceUnit

var head:Sprite;
var torso:Sprite;
var leg_r:Sprite;
var leg_l:Sprite;

func _post_setup():
	head = _skeleton.get_node("Head");
	torso = _skeleton.get_node("Torso");
	leg_l = _skeleton.get_node("Leg_L");
	leg_r = _skeleton.get_node("Leg_R");


func load_data(data:Dictionary):
	head.set_texture(load("res://sprites/sheet_01_godot.sprites/sheet_01/characters/"+data['sprite_id_head']+".tres"));
	torso.set_texture(load("res://sprites/sheet_01_godot.sprites/sheet_01/characters/"+data['sprite_id_torso']+".tres"));
	leg_r.set_texture(load("res://sprites/sheet_01_godot.sprites/sheet_01/characters/"+data['sprite_id_leg_r']+".tres"));
	leg_l.set_texture(load("res://sprites/sheet_01_godot.sprites/sheet_01/characters/"+data['sprite_id_leg_l']+".tres"));
	return;